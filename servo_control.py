import serial
import time


def version():
    serialPort.write(b"!SCVER?\r\n")
    
    # wait for data
    while(serialPort.in_waiting == 0):
        pass
    
    if(serialPort.in_waiting > 0):
        # because rx and tx lines are tied together read data that was just sent
        serialString = serialPort.readline()
        
        # read version data
        serialString = serialPort.read(3)
        return serialString


def servo_position(channel, ramp, position):
    #“!SC” C R pw.LOWBYTE, pw.HIGHBYTE, $0D
    
    string = b'!SC' + channel.to_bytes(1, 'big') + ramp.to_bytes(1, 'big') + position.to_bytes(2, 'little') + b'\r'
    
    serialPort.write(string)


def set_baudrate():
    b'!SCSBR'


def get_position(channel):
    serialPort.write(b'!SCRSP' + channel.to_bytes(1, 'big') + b'\r')
    
    # wait for data
    while(serialPort.in_waiting == 0):
        pass
        
    if(serialPort.in_waiting > 0):
        # because rx and tx lines are tied together read data that was just sent
        serialString = serialPort.read_until(b'\r')
        print(serialString)
        serialString = serialPort.read_until(b'\r')
        print(serialString)
        # read version data
        serialString = serialPort.readline()
        
        return (serialString[1] << 8) | serialString[2]


if __name__=="__main__":
    
    serialPort = serial.Serial(port = '/dev/ttyUSB0', baudrate=2400, bytesize=8,
                    timeout=2, stopbits=serial.STOPBITS_TWO)
    
    serialPort.flush()
    
    print(version())
    
    servo_position(0, 7, 500)
    time.sleep(2)
    print(get_position(0))
    
    servo_position(0, 7, 750)
    time.sleep(2)
    print(get_position(0))
    
    
    servo_position(0, 7, 1000)
    time.sleep(2)
    print(get_position(0))
    
    
    servo_position(0, 7, 750)
    time.sleep(2)
    print(get_position(0))